const ENERGY_BILL = 'energy_bill_amt';
const USER_HOUSE = 'user_house_type';
const COMPARE_HOUSE = 'house_type_compare_against';

const STD_DETACHED_SINGLE_FAMILY = "std_detached_single_family";
const HE_DETACHED_SINGLE_FAMILY = "he_detached_single_family";
const STD_ATTACHED_SINGLE_FAMILY = "std_attached_single_family";
const HE_ATTACHED_SINGLE_FAMILY = "he_attached_single_family";
const STD_MULTI_FAMILY = "std_multi_family";
const HE_MULTI_FAMILY = "he_multi_family";
const STD_SINGLE_FAMILY_CARBON = 40;

const SEPARATOR = ":";


var HOUSE_COMPARISONS = {};
var HOUSE_KEY_TO_STR = {};

function buildHCKey(key1, key2) {
  return key1 + SEPARATOR + key2;
}

function areNumsAlmostEqual(num1, num2) {
  return Math.abs( num1 - num2 ) < Number.EPSILON;
}

function buildComparisonDict() {
  // Standard detached single family
  HOUSE_COMPARISONS[buildHCKey(STD_DETACHED_SINGLE_FAMILY, STD_DETACHED_SINGLE_FAMILY)] = 1.0;
  HOUSE_COMPARISONS[buildHCKey(STD_DETACHED_SINGLE_FAMILY, HE_DETACHED_SINGLE_FAMILY)] = 1.24;
  HOUSE_COMPARISONS[buildHCKey(STD_DETACHED_SINGLE_FAMILY, STD_ATTACHED_SINGLE_FAMILY)] = 1.21;
  HOUSE_COMPARISONS[buildHCKey(STD_DETACHED_SINGLE_FAMILY, HE_ATTACHED_SINGLE_FAMILY)] =  1.52;
  HOUSE_COMPARISONS[buildHCKey(STD_DETACHED_SINGLE_FAMILY, STD_MULTI_FAMILY)] =  2.00;
  HOUSE_COMPARISONS[buildHCKey(STD_DETACHED_SINGLE_FAMILY, HE_MULTI_FAMILY)] = 2.45;

  // High efficiency detached single family
  HOUSE_COMPARISONS[buildHCKey(HE_DETACHED_SINGLE_FAMILY, STD_DETACHED_SINGLE_FAMILY)] = 0.8;
  HOUSE_COMPARISONS[buildHCKey(HE_DETACHED_SINGLE_FAMILY, HE_DETACHED_SINGLE_FAMILY)] = 1.0;
  HOUSE_COMPARISONS[buildHCKey(HE_DETACHED_SINGLE_FAMILY, STD_ATTACHED_SINGLE_FAMILY)] = 1.03;
  HOUSE_COMPARISONS[buildHCKey(HE_DETACHED_SINGLE_FAMILY, HE_ATTACHED_SINGLE_FAMILY)] = 1.22;
  HOUSE_COMPARISONS[buildHCKey(HE_DETACHED_SINGLE_FAMILY, STD_MULTI_FAMILY)] = 1.61;
  HOUSE_COMPARISONS[buildHCKey(HE_DETACHED_SINGLE_FAMILY, HE_MULTI_FAMILY)] = 1.98;

  // Standard attached single family
  HOUSE_COMPARISONS[buildHCKey(STD_ATTACHED_SINGLE_FAMILY, STD_DETACHED_SINGLE_FAMILY)] = 0.82;
  HOUSE_COMPARISONS[buildHCKey(STD_ATTACHED_SINGLE_FAMILY, HE_DETACHED_SINGLE_FAMILY)] = 1.02;
  HOUSE_COMPARISONS[buildHCKey(STD_ATTACHED_SINGLE_FAMILY, STD_ATTACHED_SINGLE_FAMILY)] = 1.0;
  HOUSE_COMPARISONS[buildHCKey(STD_ATTACHED_SINGLE_FAMILY, HE_ATTACHED_SINGLE_FAMILY)] = 1.25;
  HOUSE_COMPARISONS[buildHCKey(STD_ATTACHED_SINGLE_FAMILY, STD_MULTI_FAMILY)] = 1.65;
  HOUSE_COMPARISONS[buildHCKey(STD_ATTACHED_SINGLE_FAMILY, HE_MULTI_FAMILY)] = 2.02;

  // High efficiency attached single family
  HOUSE_COMPARISONS[buildHCKey(HE_ATTACHED_SINGLE_FAMILY, STD_DETACHED_SINGLE_FAMILY)] = 0.66;
  HOUSE_COMPARISONS[buildHCKey(HE_ATTACHED_SINGLE_FAMILY, HE_DETACHED_SINGLE_FAMILY)] = 0.81;
  HOUSE_COMPARISONS[buildHCKey(HE_ATTACHED_SINGLE_FAMILY, STD_ATTACHED_SINGLE_FAMILY)] = 0.79;
  HOUSE_COMPARISONS[buildHCKey(HE_ATTACHED_SINGLE_FAMILY, HE_ATTACHED_SINGLE_FAMILY)] = 1.0;
  HOUSE_COMPARISONS[buildHCKey(HE_ATTACHED_SINGLE_FAMILY, STD_MULTI_FAMILY)] = 1.31;
  HOUSE_COMPARISONS[buildHCKey(HE_ATTACHED_SINGLE_FAMILY, HE_MULTI_FAMILY)] = 1.61;

  // Standard multi family
  HOUSE_COMPARISONS[buildHCKey(STD_MULTI_FAMILY, STD_DETACHED_SINGLE_FAMILY)] = 0.50;
  HOUSE_COMPARISONS[buildHCKey(STD_MULTI_FAMILY, HE_DETACHED_SINGLE_FAMILY)] = 0.62;
  HOUSE_COMPARISONS[buildHCKey(STD_MULTI_FAMILY, STD_ATTACHED_SINGLE_FAMILY)] = 0.61;
  HOUSE_COMPARISONS[buildHCKey(STD_MULTI_FAMILY, HE_ATTACHED_SINGLE_FAMILY)] = 0.76;
  HOUSE_COMPARISONS[buildHCKey(STD_MULTI_FAMILY, STD_MULTI_FAMILY)] = 1.0;
  HOUSE_COMPARISONS[buildHCKey(STD_MULTI_FAMILY, HE_MULTI_FAMILY)] = 1.23;

  // High efficiency multi family
  HOUSE_COMPARISONS[buildHCKey(HE_MULTI_FAMILY, STD_DETACHED_SINGLE_FAMILY)] = 0.41;
  HOUSE_COMPARISONS[buildHCKey(HE_MULTI_FAMILY, HE_DETACHED_SINGLE_FAMILY)] = 0.59;
  HOUSE_COMPARISONS[buildHCKey(HE_MULTI_FAMILY, STD_ATTACHED_SINGLE_FAMILY)] = 0.49;
  HOUSE_COMPARISONS[buildHCKey(HE_MULTI_FAMILY, HE_ATTACHED_SINGLE_FAMILY)] = 0.62;
  HOUSE_COMPARISONS[buildHCKey(HE_MULTI_FAMILY, STD_MULTI_FAMILY)] = 0.81;
  HOUSE_COMPARISONS[buildHCKey(HE_MULTI_FAMILY, HE_MULTI_FAMILY)] = 1.0;

  HOUSE_KEY_TO_STR[STD_DETACHED_SINGLE_FAMILY] = "Detached Single Family (Standard)";
  HOUSE_KEY_TO_STR[HE_DETACHED_SINGLE_FAMILY] = "Single Family (High Efficiency)";

  HOUSE_KEY_TO_STR[STD_ATTACHED_SINGLE_FAMILY] = "Attached Single Family (Standard)";
  HOUSE_KEY_TO_STR[HE_ATTACHED_SINGLE_FAMILY] = "Attached Single Family (High Efficiency)";

  HOUSE_KEY_TO_STR[STD_MULTI_FAMILY] = "Multi family (Standard)";
  HOUSE_KEY_TO_STR[HE_MULTI_FAMILY] = "Multi family (High Efficiency)";
}

function getCarbonVal(houseTypeKey) {
  return HOUSE_COMPARISONS[buildHCKey(houseTypeKey, STD_DETACHED_SINGLE_FAMILY)] * STD_SINGLE_FAMILY_CARBON;
}

function getQsVal(param) {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  return urlParams.get(param);
}

function getComparisonRate(userHouse, compareHouse) {
  return HOUSE_COMPARISONS[buildHCKey(compareHouse, userHouse)];
}

function getEnergyBillDifference() {
  "use strict";
  let userHouse = getQsVal(USER_HOUSE);
  let toCompareTo = getQsVal(COMPARE_HOUSE);
  let currentBill = Number(getQsVal(ENERGY_BILL)).toFixed(2);
  let comparisonBill = (currentBill * getComparisonRate(userHouse, toCompareTo)).toFixed(2);


  let summary_text = "";

  let your_house_full_name = HOUSE_KEY_TO_STR[userHouse];
  let your_energy_bill = "$" + currentBill;
  let your_house_emissions = getCarbonVal(userHouse);

  let compared_to_house_full_name = HOUSE_KEY_TO_STR[toCompareTo];
  let compared_to_energy_bill = "$" + comparisonBill;
  let compared_to_emissions = getCarbonVal(toCompareTo);

  if (areNumsAlmostEqual(comparisonBill, currentBill)) {
    summary_text = "Two compared house types are the same! They're both equally efficient: " + your_house_full_name;
  } else if (comparisonBill > currentBill) {
    summary_text = your_house_full_name + " is in general <b>more efficient</b> than house type " + compared_to_house_full_name + " you compared against";
  } else  {
    summary_text = your_house_full_name + " is in general <b>less efficient</b> than house type " + compared_to_house_full_name + " you compared against";
  }
  document.getElementById("summary").innerHTML = summary_text;

  document.getElementById("your_house").innerHTML = "Your: " + your_house_full_name;
  document.getElementById("your_energy_bill").innerHTML = "Current bill: " + your_energy_bill;
  document.getElementById("your_energy_emissions").innerHTML = your_house_emissions + " tons of CO2 per year";

  document.getElementById("compared_to").innerHTML = "Compared to: " + compared_to_house_full_name;
  document.getElementById("comparison_energy_bill").innerHTML = "Compared to: " + compared_to_energy_bill;
  document.getElementById("comparison_energy_emissions").innerHTML = compared_to_emissions + " tons of CO2 per year"
}

window.onload = function() {
  buildComparisonDict();
  getEnergyBillDifference();
};
